import validators as val


class Ship:

    @staticmethod
    def ship_builder(board, str_coord, direction, length, own_ships=None):
        """
        Returns ship :obj for given start point :str f.e 'A1', direction :str N/S/W/E and length :int
        checks if ship is within game board limits - if not, returns False
        """
        # indexing given ship_coords for ship body creation loop
        if own_ships is None:
            own_ships = []
        start_index = val.str_coord_to_index_coord(board, str_coord)

        # check if ship is on the game board
        if val.is_ship_on_board(board, start_index, direction, length) is False:
            return False

        ship_coords_int = []
        for i in range(length):
            if direction == "N":
                ship_coords_int.append((start_index[0] - i, start_index[1]))
            elif direction == "S":
                ship_coords_int.append((start_index[0] + i, start_index[1]))
            elif direction == "W":
                ship_coords_int.append((start_index[0], start_index[1] - i))
            elif direction == "E":
                ship_coords_int.append((start_index[0], start_index[1] + i))

        if val.is_ship_near_others(board, ship_coords_int, own_ships) is False:
            return False

        ship_coords = [val.index_coord_to_str_coord(board, (x, y)) for x, y in ship_coords_int]

        return Ship(ship_coords, length)

    def __init__(self, coords, length):
        self.coords = coords
        self.health = length
