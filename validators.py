def strip_and_caps_str(input_str):
    """ Returns stripped and upper cased string for a given string"""
    return input_str.strip().upper()


def str_coord_to_index_coord(board, coord):
    """ Translates :str coordinate f.e. 'B3' to index :int values in :tuple f.e. (1,2) for a board :obj """
    return board.rows.index(coord[0]), board.columns.index(coord[1:])


def index_coord_to_str_coord(board, coord):
    """ Translates :int coordinate :tuple f.e. (1,2)' to :str value f.e. 'B3'' for a board :obj """
    return "".join([board.rows[coord[0]], board.columns[coord[1]]])


def get_padded_coords_for_coord(coord):
    """ For given index coord :tuple return :list of padded coords for that coord"""
    return [(x, y) for x in range(coord[0] - 1, coord[0] + 2) for y in range(coord[1] - 1, coord[1] + 2)]


def is_ship_on_board(board, start_index, direction, length):
    """ Checks if given ship setup arguments are within given game board :obj """
    if direction == "N" and start_index[0] - (length - 1) < 0:
        return False
    elif direction == "S" and start_index[0] + (length - 1) > board.board_size_index[0]:
        return False
    elif direction == "W" and start_index[1] - (length - 1) < 0:
        return False
    elif direction == "E" and start_index[1] + (length - 1) > board.board_size_index[1]:
        return False
    return True


def is_coord_on_board(board, coord):
    """ Checks if given coord :tuple (x,y) are within bounds of given game board :obj"""
    return (0 <= coord[0] <= board.board_size_index[0]) and (0 <= coord[1] <= board.board_size_index[1])


def is_ship_near_others(board, ship_coords_int, own_ships=None):
    """
    Checks if passed ship coordinates in :int format are not touching other ships passed in as a :list of Ship :objects
    Requires parsing board :object for translating coordinates
    Returns True if given ship is not touching other ship, False if it does
    """
    if own_ships is None:
        own_ships = list()
    # getting existing ships coords
    own_ships_str_coords = sum((i.coords for i in own_ships), [])
    own_ships_index_coords = [str_coord_to_index_coord(board, x) for x in own_ships_str_coords]

    # check for overlapping build ship coords with existing ships coords
    for coord in ship_coords_int:
        if coord in own_ships_index_coords:
            return False

    padd_coord_nest = [get_padded_coords_for_coord((i, j)) for (i, j) in ship_coords_int]
    padded_coords = {x for y in padd_coord_nest for x in y}

    # checking every padded coordinates for collision with other ships, omitting own body
    for (x, y) in padded_coords:
        if (x, y) in ship_coords_int:
            continue
        if (x, y) in own_ships_index_coords:
            return False

    return True
