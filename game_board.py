import validators as val


class Board:

    def __init__(self):
        self.columns = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
        self.rows = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
        self.placeholders = [[" " for _ in range(len(self.columns))]for _ in range(len(self.rows))]
        self.shots = []
        self.ships = []
        self.game_map = {}
        self.board_size_index = len(self.rows) - 1, len(self.columns) - 1

    def making_shots_coordinates(self):
        """Making index tuples related to rows/cols by splitting shots"""

        shots_split = [(shot[0], shot[1:]) for shot in self.shots]
        return map(
            lambda x: (self.rows.index(x[0]), self.columns.index(x[1])),
            shots_split,
        )

    def mark_shots(self):
        """return shot markers instead of dots placeholder"""
        shot_coordinates = self.making_shots_coordinates()
        fleet_idx_coordinates = [val.str_coord_to_index_coord(self, x) for x in self.ships]

        for cord in shot_coordinates:
            if cord in fleet_idx_coordinates:
                self.placeholders[cord[0]][cord[1]] = 'X'
            else:
                self.placeholders[cord[0]][cord[1]] = 'o'
        return self.placeholders

    def mark_ships(self):
        """set '#' mark for ship tile in self.placeholders"""
        fleet_idx_coordinates = [val.str_coord_to_index_coord(self, x) for x in self.ships]
        for cord in fleet_idx_coordinates:
            self.placeholders[cord[0]][cord[1]] = "[:]"
        return self.placeholders

    def reset_board_marks(self):
        self.placeholders = [[" " for _ in range(len(self.columns))] for _ in range(len(self.rows))]

    def draw_board(self):
        for n, letter in enumerate(self.rows):
            self.game_map[letter] = self.placeholders[n]

        row_splitter = ("---+" * len(self.columns) + "-" * 4)
        print("   |" + ("|".join(col.center(3) for col in self.columns)) + "|")
        print(row_splitter)

        for key, value in self.game_map.items():
            print("{:<3s}".format(key) + "|" + "|".join(box.center(3) for box in value) + "|")
            print(row_splitter)
